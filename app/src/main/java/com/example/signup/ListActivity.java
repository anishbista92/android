package com.example.signup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.widget.ListView;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private ListView listView;

    private ArrayList<Product> productList = new ArrayList<>();

    private ProdutListAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        initToolbar();
        findViews();
    }

    private void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Product List");

    }


    private void findViews(){
        listView = findViewById(R.id.listView);
        setData();
        adapter = new ProdutListAdapter(productList,ListActivity.this);
        listView.setAdapter(adapter);
    }


    private void setData(){
        Product p1 = new Product();
        Product p2 = new Product();
        Product p3 = new Product();
        Product p4 = new Product();

        p1.setTitle("account");
        p1.setProductDescription("hello hi hello hi hello hi hello hi");
        p1.setUnit("Pcs");
        p1.setQuantity("100");
        p1.setPrice("150");
        p1.setImage(R.drawable.account);


        p2.setTitle("home");
        p2.setProductDescription("hello hi hello hi hello hi hello hi");
        p2.setUnit("Pc");
        p2.setQuantity("10");
        p2.setPrice("450");
        p2.setImage(R.drawable.home);

        p3.setTitle("logo");
        p3.setProductDescription("hello hi hello hi hello hi hello hi" );
        p3.setUnit("Pcs");
        p3.setQuantity("1000");
        p3.setPrice("650");
        p3.setImage(R.drawable.logo);

        p4.setTitle("message");
        p4.setProductDescription("hello hi hello hi hello hi hello hi");
        p4.setUnit("Pcs");
        p4.setQuantity("500");
        p4.setPrice("500");
        p4.setImage(R.drawable.message);

        productList.add(p1);
        productList.add(p2);
        productList.add(p3);
        productList.add(p4);


    }
}