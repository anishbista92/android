package com.example.signup;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;


public class HomeFragment extends Fragment {
    private RadioGroup rdGender;
//    private RadioButton rdMale,rdFemale;
    private Button btnSend;
    private View mainview;
    private CheckBox chkJava,chkPython,chkJavaScript;



    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainview =  inflater.inflate(R.layout.fragment_home, container, false);
        findview();
        return mainview;
    }

    private void findview(){
        rdGender = mainview.findViewById(R.id.rdgGender);
        btnSend = mainview.findViewById(R.id.btnSend);
        chkJava = mainview.findViewById(R.id.chkJava);
        chkPython = mainview.findViewById(R.id.chkPython);
        chkJavaScript = mainview.findViewById(R.id.chkJavaScript);
        btnSend.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                String skill = " ";
                if(chkJava.isChecked()){
                    skill = skill +" "+ chkJava.getText().toString();
                }if(chkPython.isChecked()){
                    skill = skill +", "+ chkPython.getText().toString();
                }if(chkJavaScript.isChecked()){
                    skill = skill+ ", "+ chkJavaScript.getText().toString();
                }

                String value = ((RadioButton) mainview.findViewById(rdGender.getCheckedRadioButtonId())).getText().toString();
                Toast.makeText(getContext(),value,Toast.LENGTH_SHORT).show();
                Toast.makeText(getContext(),"You have choosen"+skill,Toast.LENGTH_SHORT).show();
            }
        });
//        rdMale = view.findViewById(R.id.rdMale);
//        rdFemale = view.findViewById(R.id.rdFemale);

    }
}