package com.example.signup;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ProductDataVH extends RecyclerView.ViewHolder{
    TextView txtProductTitle, txtProductDescription, txtProductUnit, txtProductQuantity, txtProductPrice;
    ImageView imgProduct;
    public ProductDataVH(@NonNull View view) {
        super(view);
        txtProductTitle = view.findViewById(R.id.textProductTitle);
        txtProductDescription = view.findViewById(R.id.textProductDescription);
        txtProductUnit = view.findViewById(R.id.textProductUnit);
        txtProductQuantity = view.findViewById(R.id.textProductQuantity);
        txtProductPrice = view.findViewById(R.id.textProductPrice);
        imgProduct = view.findViewById(R.id.imgProduct);
    }
}