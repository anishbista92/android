package com.example.signup;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class ProductAdapter extends RecyclerView.Adapter<ProductDataVH> {

    private ArrayList<Product> productList = new ArrayList<>();
    private Context context;
    ProductAdapter(ArrayList<Product> dataList, Context mContext){
        productList.clear();
        productList.addAll(dataList);
        context = mContext;
    }

    @NonNull
    @Override
    public ProductDataVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_product_item, parent, false);

        return new ProductDataVH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductDataVH holder, int position) {
        holder.txtProductTitle.setText(productList.get(position).getTitle());
        holder.txtProductDescription.setText(productList.get(position).getProductDescription());
        holder.txtProductUnit.setText("Unit: " + productList.get(position).getUnit());
        holder.txtProductQuantity.setText("Quantity: " + productList.get(position).getQuantity());
        holder.txtProductPrice.setText("Price: Rs." + productList.get(position).getPrice());
        holder.imgProduct.setImageDrawable(context.getResources().getDrawable(productList.get(position).getImage()));

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }
}


