package com.example.signup;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class ProductRecyclerActivity extends AppCompatActivity {
    private RecyclerView productRecycler;
    private Toolbar toolbar;
    private ProductAdapter productAdapter;
    private ArrayList<Product> product = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_recycler);
        setData();

        initToolbar();
        findViews();
    }
    private void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Product List Page");
    }
    private void findViews(){
        productRecycler = findViewById(R.id.productRecycler);
        productAdapter = new ProductAdapter(product,ProductRecyclerActivity.this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
//        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        productRecycler.setLayoutManager(layoutManager);
        productRecycler.setAdapter(productAdapter);

    }



    private void setData() {
        Product p1 = new Product();
        Product p2 = new Product();
        Product p3 = new Product();
        Product p4 = new Product();
        p1.setTitle("fire extinguisher");
        p1.setProductDescription("product detail of fire extinguisher");
        p1.setPrice("999");
        p1.setUnit("50");
        p1.setImage(R.drawable.message);


        p2.setTitle(" ChinaCup");
        p2.setProductDescription("product detail of China cup");
        p2.setPrice("299");
        p2.setUnit("50");
        p2.setImage(R.drawable.add);

        p3.setTitle(" HeadPhones");
        p3.setProductDescription("product detail of headphones");//alt +  + L
        p3.setPrice("299");
        p3.setUnit("50");
        p3.setImage(R.drawable.button);

        product.add(p1);
        product.add(p2);
        product.add(p2);




    }

}